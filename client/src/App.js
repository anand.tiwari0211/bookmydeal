import React,{ Fragment } from 'react'
import './App.css'
import AppRoutes from './routes';
import Notification from './components/Notification';
import ReactModal from './containers/Modal';

const App = () => (
  <Fragment>
    <Notification autoClose={8000} />
    <AppRoutes />
    <ReactModal />
  </Fragment>
);

export default App;
