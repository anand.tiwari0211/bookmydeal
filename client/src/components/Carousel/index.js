import React, { Component } from "react";

class Carousel extends Component {
  render() {
    return (
      <section className="slider_sec">
        <div id="myCarousel" className="carousel slide" data-ride="carousel">
          <div className="carousel-inner">
            <div className="item active">
              <img src="images/slider1.jpg" alt="coupen-offers" />
            </div>

            <div className="item">
              <img src="images/slider1.jpg" alt="coupen-offers" />
            </div>

            <div className="item">
              <img src="images/slider1.jpg" alt="coupen-offers" />
            </div>
          </div>

          <a
            className="left carousel-control"
            href="#myCarousel"
            data-slide="prev"
          >
            <span className="fa fa-chevron-left" />
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="right carousel-control"
            href="#myCarousel"
            data-slide="next"
          >
            <span className="fa fa-chevron-right" />
            <span className="sr-only">Next</span>
          </a>
        </div>
      </section>
    );
  }
}

export default Carousel;
