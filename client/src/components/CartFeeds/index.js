import React, { Component, Fragment } from "react";

class CartFeeds extends Component {
  render() {
    return (
      <Fragment>
      <div className="parallax first-section" data-stellar-background-ratio="0.5">

                  <div className="container">

                      <div className="section-title m30 text-center">

                          <h1>Shopping Cart</h1>

                          <p>In dignissim feugiat gravida. Proin feugiat quam sed gravida fringilla. Proin quis mauris ut magna fringilla vulputate quis non ante. Integer bibendum velit dui. Sed consequat nisi id convallis eleifend. </p>

                      </div>

                  </div>

              </div>

      <main className="page">

          <section className="shopping-cart dark">

            <div className="container">

                  <div className="content">

                <div className="row">

                  <div className="col-md-12 col-lg-8">

                    <div className="items">

                      <div className="product">

                        <div className="row">

                          <div className="col-12 col-sm-3 col-md-3">

                            <img className="img-fluid mx-auto d-block image cart_thumb" src="images/promo_02.png"/>

                          </div>

                          <div className="col-12 col-sm-8 col-md-8">

                            <div className="info">

                              <div className="row">

                                <div className="col-12 col-sm-5 col-md-5 product-name">

                                  <div className="product-name">

                                    <a href="#">Lorem Ipsum dolor</a>

                                    <div className="product-info">

                                      <div>Display: <span className="value">5 inch</span></div>

                                      <div>RAM: <span className="value">4GB</span></div>

                                      <div>Memory: <span className="value">32GB</span></div>

                                    </div>

                                  </div>

                                </div>

                                <div className="col-12 col-sm-4 col-md-4 quantity">

                                  <label for="quantity">Quantity:</label>

                                  <input id="quantity" type="number" value ="1" className="form-control quantity-input"/>

                                </div>

                                <div className="col-12 col-sm-3 col-md-3 price">

                                  <span>$120</span>

                                </div>

                              </div>

                            </div>

                          </div>

                        </div>

                      </div>

                      <div className="product">

                        <div className="row">

                          <div className="col-12 col-sm-3 col-md-3">

                            <img className="img-fluid mx-auto d-block image cart_thumb" src="images/promo_02.png"/>

                          </div>

                          <div className="col-12 col-sm-8 col-md-8">

                            <div className="info">

                              <div className="row">

                                <div className="col-12 col-sm-5 col-md-5 product-name">

                                  <div className="product-name">

                                    <a href="#">Lorem Ipsum dolor</a>

                                    <div className="product-info">

                                      <div>Display: <span className="value">5 inch</span></div>

                                      <div>RAM: <span className="value">4GB</span></div>

                                      <div>Memory: <span className="value">32GB</span></div>

                                    </div>

                                  </div>

                                </div>

                                <div className="col-12 col-sm-4 col-md-4 quantity">

                                  <label for="quantity">Quantity:</label>

                                  <input id="quantity" type="number" value ="1" className="form-control quantity-input"/>

                                </div>

                                <div className="col-12 col-sm-3 col-md-3 price">

                                  <span>$120</span>

                                </div>

                              </div>

                            </div>

                          </div>

                        </div>

                      </div>

                      <div className="product">

                        <div className="row">

                          <div className="col-12 col-sm-3 col-md-3">

                            <img className="img-fluid mx-auto d-block image cart_thumb" src="images/promo_02.png"/>

                          </div>

                          <div className="col-12 col-sm-8 col-md-8">

                            <div className="info">

                              <div className="row">

                                <div className="col-12 col-sm-5 col-md-5 product-name">

                                  <div className="product-name">

                                    <a href="#">Lorem Ipsum dolor</a>

                                    <div className="product-info">

                                      <div>Display: <span className="value">5 inch</span></div>

                                      <div>RAM: <span className="value">4GB</span></div>

                                      <div>Memory: <span className="value">32GB</span></div>

                                    </div>

                                  </div>

                                </div>

                                <div className="col-12 col-sm-4 col-md-4 quantity">

                                  <label for="quantity">Quantity:</label>

                                  <input id="quantity" type="number" value ="1" className="form-control quantity-input"/>

                                </div>

                                <div className="col-12 col-sm-3 col-md-3 price">

                                  <span>$120</span>

                                </div>

                              </div>

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                  <div className="col-md-12 col-lg-4">

                    <div className="summary">

                      <h3>Summary</h3>

                      <div className="summary-item"><span className="text">Subtotal</span><span className="price">$360</span></div>

                      <div className="summary-item"><span className="text">Discount</span><span className="price">$0</span></div>

                      <div className="summary-item"><span className="text">Shipping</span><span className="price">$0</span></div>

                      <div className="summary-item"><span className="text">Total</span><span className="price">$360</span></div>

                      <a href="Checkout.html"><button type="button" className="btn btn-primary btn-lg btn-block">Checkout</button></a>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </section>

        </main>
      </Fragment>
    );
  }
}

export default CartFeeds;
