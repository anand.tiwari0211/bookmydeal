import React, { Component, Fragment } from "react";

class CompetetionDetail extends Component {
  render() {
    return (
      <Fragment>
        <div className="parallax first-section detail_parallex detail_cmp_bnr">
          <div className="container">
            <div className="section-title m30 text-center">
              <h1>Ninja Rescue Game</h1>

              <p>
                In dignissim feugiat gravida. Proin feugiat quam sed gravida
                fringilla. Proin quis mauris ut magna fringilla vulputate quis
                non ante. Integer bibendum velit dui. Sed consequat nisi id
                convallis eleifend.{" "}
              </p>
            </div>
          </div>
        </div>

        <div className="about_wrap_comp">
          <div className="container">
            <div className="row">
              <div className="col-md-5 col-lg-5 col-sm-12">
                <div className="img_sec">
                  <div className="fet_txt">
                    <h3>Competition Detail</h3>
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s, when an unknown
                      printer.
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md-7 col-lg-7 col-sm-12">
                <div className="row">
                  <div className="col-md-6 col-sm-12">
                    <div
                      className="icn_txt wow fadeInUp"
                      data-wow-duration="1s"
                      data-wow-delay="1s"
                    >
                      <div className="icon1">
                        <span className="fa fa-usd" />
                      </div>
                      <div className="txt2">
                        <h4 className="theme_color">COUPON PRICE</h4>
                        <strong>$ 20</strong>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 col-sm-12">
                    <div
                      className="icn_txt wow fadeInUp"
                      data-wow-duration="1s"
                      data-wow-delay="2s"
                    >
                      <div className="icon1">
                        <span className="fa fa-ticket" />
                      </div>
                      <div className="txt2">
                        <h4 className="theme_color">Coupons Availability</h4>
                        <strong>Yes</strong>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 col-sm-12">
                    <div
                      className="icn_txt wow fadeInUp"
                      data-wow-duration="1s"
                      data-wow-delay="3s"
                    >
                      <div className="icon1">
                        <span className="fa fa-map-marker" />
                      </div>
                      <div className="txt2">
                        <h4 className="theme_color">Address</h4>
                        <strong>London</strong>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 col-sm-12">
                    <div
                      className="icn_txt wow fadeInUp"
                      data-wow-duration="1s"
                      data-wow-delay="4s"
                    >
                      <div className="icon1">
                        <span className="fa fa-users" />
                      </div>
                      <div className="txt2">
                        <h4 className="theme_color">Other Participants</h4>
                        <strong>5 Out Of 32</strong>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="clearfix" />
              </div>
            </div>
          </div>
        </div>
        <section className="awrd_pric section">
          <div className="container">
            <div className="pricing_sec">
              <div className="text-center site_heading">
                <h3>Winning Prize</h3> <br />
              </div>
              <div className="row">
                <div className="col-md-4 wow bounceIn" data-wow-delay="1.0s">
                  <div className="dimnd_bx">
                    <div className="txt_insd">
                      <h1 className="extra_hdng"> 1st</h1>
                      <p className="pric_sb_hd">$1000</p>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 wow bounceIn" data-wow-delay="2.0s">
                  <div className="dimnd_bx">
                    <div className="txt_insd">
                      <h1 className="extra_hdng"> 2nd</h1>
                      <p className="pric_sb_hd">$800</p>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 wow bounceIn" data-wow-delay="3.0s">
                  <div className="dimnd_bx">
                    <div className="txt_insd">
                      <h1 className="extra_hdng"> 3rd</h1>
                      <p className="pric_sb_hd">$600</p>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 wow bounceIn" data-wow-delay="4.0s">
                  <div className="dimnd_bx">
                    <div className="txt_insd">
                      <h1 className="extra_hdng"> 4th</h1>
                      <p className="pric_sb_hd">$400</p>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 wow bounceIn" data-wow-delay="5.0s">
                  <div className="dimnd_bx">
                    <div className="txt_insd">
                      <h1 className="extra_hdng"> 5th</h1>
                      <p className="pric_sb_hd">$200</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <div className="prticipate">
          <div className="container">
            <div className="text-center site_heading">
              <h3>Other Participants</h3>
            </div>
            <div
              id="othrParticipnt"
              className="carousel slide"
              data-ride="carousel"
            >
              <div className="carousel-inner">
                <div className="item">
                  <div className="row">
                    <div className="col-md-4 col-sm-12">
                      <div className="grid_fll">
                        <div className="coupon-list list-wrapper">
                          <div className="coupon-wrapper">
                            <div className="post-media text-center">
                              <a href="#">
                                <img
                                  src="images/promo_01.png"
                                  alt=""
                                  className="img-responsive"
                                />
                              </a>
                              <small>
                                <a href="#">View Store Coupons</a>
                              </small>
                            </div>

                            <div className="coupon-meta">
                              <h5 className="theme_color">
                                <a href="#">50% Discount Coupon</a>
                              </h5>
                              <p>
                                At reasonable prices, quality assurance, 100%
                                secure shopping.
                              </p>
                            </div>

                            <div className="showcode">
                              <a href="#" className="code-link">
                                <span
                                  className="coupon-code"
                                  data-original-title=""
                                  title=""
                                />
                                <span className="show-code print-code">
                                  <i className="fa fa-eye" /> View More
                                </span>
                              </a>
                            </div>

                            <div className="coupon-bottom clearfix">
                              <small className="pull-left">
                                Expire : 21/07/2016
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-12">
                      <div className="grid_fll">
                        <div className="coupon-list list-wrapper">
                          <div className="coupon-wrapper">
                            <div className="post-media text-center">
                              <a href="#">
                                <img
                                  src="images/promo_01.png"
                                  alt=""
                                  className="img-responsive"
                                />
                              </a>
                              <small>
                                <a href="#">View Store Coupons</a>
                              </small>
                            </div>

                            <div className="coupon-meta">
                              <h5 className="theme_color">
                                <a href="#">50% Discount Coupon</a>
                              </h5>
                              <p>
                                At reasonable prices, quality assurance, 100%
                                secure shopping.
                              </p>
                            </div>

                            <div className="showcode">
                              <a href="#" className="code-link">
                                <span
                                  className="coupon-code"
                                  data-original-title=""
                                  title=""
                                />
                                <span className="show-code print-code">
                                  <i className="fa fa-eye" /> View More
                                </span>
                              </a>
                            </div>

                            <div className="coupon-bottom clearfix">
                              <small className="pull-left">
                                Expire : 21/07/2016
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-12">
                      <div className="grid_fll">
                        <div className="coupon-list list-wrapper">
                          <div className="coupon-wrapper">
                            <div className="post-media text-center">
                              <a href="#">
                                <img
                                  src="images/promo_01.png"
                                  alt=""
                                  className="img-responsive"
                                />
                              </a>
                              <small>
                                <a href="#">View Store Coupons</a>
                              </small>
                            </div>

                            <div className="coupon-meta">
                              <h5 className="theme_color">
                                <a href="#">50% Discount Coupon</a>
                              </h5>
                              <p>
                                At reasonable prices, quality assurance, 100%
                                secure shopping.
                              </p>
                            </div>

                            <div className="showcode">
                              <a href="#" className="code-link">
                                <span
                                  className="coupon-code"
                                  data-original-title=""
                                  title=""
                                />
                                <span className="show-code print-code">
                                  <i className="fa fa-eye" /> View More
                                </span>
                              </a>
                            </div>

                            <div className="coupon-bottom clearfix">
                              <small className="pull-left">
                                Expire : 21/07/2016
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="item active">
                  <div className="row">
                    <div className="col-md-4 col-sm-12">
                      <div className="grid_fll">
                        <div className="coupon-list list-wrapper">
                          <div className="coupon-wrapper">
                            <div className="post-media text-center">
                              <a href="#">
                                <img
                                  src="images/promo_01.png"
                                  alt=""
                                  className="img-responsive"
                                />
                              </a>
                              <small>
                                <a href="#">View Store Coupons</a>
                              </small>
                            </div>

                            <div className="coupon-meta">
                              <h5 className="theme_color">
                                <a href="#">50% Discount Coupon</a>
                              </h5>
                              <p>
                                At reasonable prices, quality assurance, 100%
                                secure shopping.
                              </p>
                            </div>

                            <div className="showcode">
                              <a href="#" className="code-link">
                                <span
                                  className="coupon-code"
                                  data-original-title=""
                                  title=""
                                />
                                <span className="show-code print-code">
                                  <i className="fa fa-eye" /> View More
                                </span>
                              </a>
                            </div>

                            <div className="coupon-bottom clearfix">
                              <small className="pull-left">
                                Expire : 21/07/2016
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-12">
                      <div className="grid_fll">
                        <div className="coupon-list list-wrapper">
                          <div className="coupon-wrapper">
                            <div className="post-media text-center">
                              <a href="#">
                                <img
                                  src="images/promo_01.png"
                                  alt=""
                                  className="img-responsive"
                                />
                              </a>
                              <small>
                                <a href="#">View Store Coupons</a>
                              </small>
                            </div>

                            <div className="coupon-meta">
                              <h5 className="theme_color">
                                <a href="#">50% Discount Coupon</a>
                              </h5>
                              <p>
                                At reasonable prices, quality assurance, 100%
                                secure shopping.
                              </p>
                            </div>

                            <div className="showcode">
                              <a href="#" className="code-link">
                                <span
                                  className="coupon-code"
                                  data-original-title=""
                                  title=""
                                />
                                <span className="show-code print-code">
                                  <i className="fa fa-eye" /> View More
                                </span>
                              </a>
                            </div>

                            <div className="coupon-bottom clearfix">
                              <small className="pull-left">
                                Expire : 21/07/2016
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-12">
                      <div className="grid_fll">
                        <div className="coupon-list list-wrapper">
                          <div className="coupon-wrapper">
                            <div className="post-media text-center">
                              <a href="#">
                                <img
                                  src="images/promo_01.png"
                                  alt=""
                                  className="img-responsive"
                                />
                              </a>
                              <small>
                                <a href="#">View Store Coupons</a>
                              </small>
                            </div>

                            <div className="coupon-meta">
                              <h5 className="theme_color">
                                <a href="#">50% Discount Coupon</a>
                              </h5>
                              <p>
                                At reasonable prices, quality assurance, 100%
                                secure shopping.
                              </p>
                            </div>

                            <div className="showcode">
                              <a href="#" className="code-link">
                                <span
                                  className="coupon-code"
                                  data-original-title=""
                                  title=""
                                />
                                <span className="show-code print-code">
                                  <i className="fa fa-eye" /> View More
                                </span>
                              </a>
                            </div>

                            <div className="coupon-bottom clearfix">
                              <small className="pull-left">
                                Expire : 21/07/2016
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="item">
                  <div className="row">
                    <div className="col-md-4 col-sm-12">
                      <div className="grid_fll">
                        <div className="coupon-list list-wrapper">
                          <div className="coupon-wrapper">
                            <div className="post-media text-center">
                              <a href="#">
                                <img
                                  src="images/promo_01.png"
                                  alt=""
                                  className="img-responsive"
                                />
                              </a>
                              <small>
                                <a href="#">View Store Coupons</a>
                              </small>
                            </div>

                            <div className="coupon-meta">
                              <h5 className="theme_color">
                                <a href="#">50% Discount Coupon</a>
                              </h5>
                              <p>
                                At reasonable prices, quality assurance, 100%
                                secure shopping.
                              </p>
                            </div>

                            <div className="showcode">
                              <a href="#" className="code-link">
                                <span
                                  className="coupon-code"
                                  data-original-title=""
                                  title=""
                                />
                                <span className="show-code print-code">
                                  <i className="fa fa-eye" /> View More
                                </span>
                              </a>
                            </div>

                            <div className="coupon-bottom clearfix">
                              <small className="pull-left">
                                Expire : 21/07/2016
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-12">
                      <div className="grid_fll">
                        <div className="coupon-list list-wrapper">
                          <div className="coupon-wrapper">
                            <div className="post-media text-center">
                              <a href="#">
                                <img
                                  src="images/promo_01.png"
                                  alt=""
                                  className="img-responsive"
                                />
                              </a>
                              <small>
                                <a href="#">View Store Coupons</a>
                              </small>
                            </div>

                            <div className="coupon-meta">
                              <h5 className="theme_color">
                                <a href="#">50% Discount Coupon</a>
                              </h5>
                              <p>
                                At reasonable prices, quality assurance, 100%
                                secure shopping.
                              </p>
                            </div>

                            <div className="showcode">
                              <a href="#" className="code-link">
                                <span
                                  className="coupon-code"
                                  data-original-title=""
                                  title=""
                                />
                                <span className="show-code print-code">
                                  <i className="fa fa-eye" /> View More
                                </span>
                              </a>
                            </div>

                            <div className="coupon-bottom clearfix">
                              <small className="pull-left">
                                Expire : 21/07/2016
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 col-sm-12">
                      <div className="grid_fll">
                        <div className="coupon-list list-wrapper">
                          <div className="coupon-wrapper">
                            <div className="post-media text-center">
                              <a href="#">
                                <img
                                  src="images/promo_01.png"
                                  alt=""
                                  className="img-responsive"
                                />
                              </a>
                              <small>
                                <a href="#">View Store Coupons</a>
                              </small>
                            </div>

                            <div className="coupon-meta">
                              <h5 className="theme_color">
                                <a href="#">50% Discount Coupon</a>
                              </h5>
                              <p>
                                At reasonable prices, quality assurance, 100%
                                secure shopping.
                              </p>
                            </div>

                            <div className="showcode">
                              <a href="#" className="code-link">
                                <span
                                  className="coupon-code"
                                  data-original-title=""
                                  title=""
                                />
                                <span className="show-code print-code">
                                  <i className="fa fa-eye" /> View More
                                </span>
                              </a>
                            </div>

                            <div className="coupon-bottom clearfix">
                              <small className="pull-left">
                                Expire : 21/07/2016
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <a
                className="left carousel-control"
                href="#othrParticipnt"
                data-slide="prev"
              >
                <span className="fa fa-chevron-left" />
                <span className="sr-only">Previous</span>
              </a>
              <a
                className="right carousel-control"
                href="#othrParticipnt"
                data-slide="next"
              >
                <span className="fa fa-chevron-right" />
                <span className="sr-only">Next</span>
              </a>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default CompetetionDetail;
