import React, { Component } from "react";

class Competition extends Component {
  render() {
    return (
      <div>
        <div
          className="tab-pane"
          id="v-pills-home2"
          role="tabpanel"
          aria-labelledby="v-pills-home-tab2"
        >
          <div id="myCarousel" className="carousel slide" data-ride="carousel">
            <div className="carousel-inner">
              <div className="item active">
                <div className="row">
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="item">
                <div className="row">
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="item">
                <div className="row">
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="grid_fll">
                      <div className="coupon-list list-wrapper">
                        <div className="coupon-wrapper">
                          <div className="post-media text-center">
                            <a href="#">
                              <img
                                src="images/promo_01.png"
                                alt=""
                                className="img-responsive"
                              />
                            </a>
                            <small>
                              <a href="#">View Store Coupons</a>
                            </small>
                          </div>

                          <div className="coupon-meta">
                            <h5 className="theme_color">
                              <a href="#">50% Discount Coupon</a>
                            </h5>
                            <p>
                              At reasonable prices, quality assurance, 100%
                              secure shopping.
                            </p>
                          </div>

                          <div className="showcode">
                            <a href="#" className="code-link">
                              <span
                                className="coupon-code"
                                data-original-title=""
                                title=""
                              />
                              <span className="show-code print-code">
                                <i className="fa fa-eye" /> View More
                              </span>
                            </a>
                          </div>

                          <div className="coupon-bottom clearfix">
                            <small className="pull-left">
                              Expire : 21/07/2016
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <a
              className="left carousel-control"
              href="#myCarousel"
              data-slide="prev"
            >
              <span className="fa fa-chevron-left" />
              <span className="sr-only">Previous</span>
            </a>
            <a
              className="right carousel-control"
              href="#myCarousel"
              data-slide="next"
            >
              <span className="fa fa-chevron-right" />
              <span className="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default Competition;
