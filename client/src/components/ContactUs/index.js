import React, { Component, Fragment } from "react";

class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.createContactRequest({ ...this.state });
    this.setState({});
  }

  render() {
    return (
      <Fragment>
        <div
          className="parallax first-section"
          data-stellar-background-ratio="0.5"
        >
          <div className="container">
            <div className="section-title m30 text-center">
              <h1>Contact Us</h1>
              <p>
                In dignissim feugiat gravida. Proin feugiat quam sed gravida
                fringilla. Proin quis mauris ut magna fringilla vulputate quis
                non ante. Integer bibendum velit dui. Sed consequat nisi id
                convallis eleifend.{" "}
              </p>
            </div>
          </div>
        </div>

        <div className="section wb">
          <div className="container">
            <div className="row">
              <div className="content col-md-7">
                <div className="post-wrapper">
                  <div className="widget-title">
                    <h4>
                      <span>Contact Form</span>
                    </h4>
                  </div>
                  <p>
                    Remaining essentially unchanged. It was popularised in the
                    1960s with the release of Letraset sheets containing Lorem
                    Ipsum passages, and more recently with desktop publishing
                    software like Aldus PageMaker including versions of Lorem
                    Ipsum.
                  </p>
                  <hr className="invis3" />

                  <form id="submit" onSubmit={this.handleSubmit} className="comment-form newsletter">
                    <div className="row">
                      <div className="col-md-6 col-sm-12">
                        <label className="control-label">Your Name</label>
                        <input
                          type="text"
                          name="name"
                          onChange={this.handleChange}
                          className="form-control"
                          placeholder=""
                          value={this.state.name}
                          required
                        />
                      </div>
                      <div className="col-md-6 col-sm-12">
                        <label className="control-label">Your Email</label>
                        <input
                          name="email"
                          type="email"
                          onChange={this.handleChange}
                          className="form-control"
                          placeholder=""
                          value={this.state.email}
                          required
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6 col-sm-12">
                        <label className="control-label">Phone</label>
                        <input
                          type="text"
                          name="phone"
                          onChange={this.handleChange}
                          className="form-control"
                          placeholder=""
                          value={this.state.phone}
                        />
                      </div>
                      <div className="col-md-6 col-sm-12">
                        <label className="control-label">Subject</label>
                        <input
                          type="text"
                          name="subject"
                          onChange={this.handleChange}
                          className="form-control"
                          placeholder=""
                          value={this.state.subject}
                          required
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-12 col-sm-12">
                        <label className="control-label">Your Message</label>
                        <textarea
                          name="message"
                          onChange={this.handleChange}
                          className="form-control"
                          placeholder=""
                          value={this.state.message}
                          required
                        />
                      </div>
                    </div>
                    <button type="submit" className="btn btn-custom">Submit Form</button>
                  </form>
                </div>
              </div>

              <div className="sidebar col-md-5">
                <div className="widget">
                  <div className="widget-title">
                    <h4>
                      <span>Contact Map</span>
                    </h4>
                  </div>
                  <div id="map" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default ContactUs;
