import React, { Component, Fragment } from "react";
import Profile from "../../containers/Profile";
import Competition from "../Competition";

class DashboardProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeLink: "Dashboard"
    };
  }

  componentDidMount() {
    this.props.getUserRequest()
  }

  handleLogout = () => {
    this.props.onLogoutRequest();
  };

  handleSideNav = e => {
    this.setState({ activeLink: e.target.name });
  };

  renderDashBoard = () => (
    <div
      className="tab-pane fade in active"
      id="v-pills-home"
      role="tabpanel"
      aria-labelledby="v-pills-home-tab"
    >
      <div className="owl-slider">
        <h3 className="theme_color">Upcoming Competition</h3>
        <div id="carousel" className="owl-carousel">
          <div className="item">
            <div className="coupon-list list-wrapper">
              <div className="coupon-wrapper">
                <div className="post-media text-center">
                  <a href="#">
                    <img
                      src="images/promo_01.png"
                      alt=""
                      className="img-responsive"
                    />
                  </a>
                  <small>
                    <a href="#">View Store Coupons</a>
                  </small>
                </div>

                <div className="coupon-meta">
                  <h5 className="theme_color">
                    <a href="#">50% Discount Coupon</a>
                  </h5>
                  <p>
                    At reasonable prices, quality assurance, 100% secure
                    shopping.
                  </p>
                </div>

                <div className="showcode">
                  <a href="#" className="code-link">
                    <span
                      className="coupon-code"
                      data-original-title=""
                      title=""
                    />
                    <span className="show-code print-code">
                      <i className="fa fa-eye" /> View More
                    </span>
                  </a>
                </div>

                <div className="coupon-bottom clearfix">
                  <small className="pull-left">Expire : 21/07/2016</small>
                </div>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="coupon-list list-wrapper">
              <div className="coupon-wrapper">
                <div className="post-media text-center">
                  <a href="#">
                    <img
                      src="images/promo_01.png"
                      alt=""
                      className="img-responsive"
                    />
                  </a>
                  <small>
                    <a href="#">View Store Coupons</a>
                  </small>
                </div>

                <div className="coupon-meta">
                  <h5 className="theme_color">
                    <a href="#">50% Discount Coupon</a>
                  </h5>
                  <p>
                    At reasonable prices, quality assurance, 100% secure
                    shopping.
                  </p>
                </div>

                <div className="showcode">
                  <a href="#" className="code-link">
                    <span
                      className="coupon-code"
                      data-original-title=""
                      title=""
                    />
                    <span className="show-code print-code">
                      <i className="fa fa-eye" /> View More
                    </span>
                  </a>
                </div>

                <div className="coupon-bottom clearfix">
                  <small className="pull-left">Expire : 21/07/2016</small>
                </div>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="coupon-list list-wrapper">
              <div className="coupon-wrapper">
                <div className="post-media text-center">
                  <a href="#">
                    <img
                      src="images/promo_01.png"
                      alt=""
                      className="img-responsive"
                    />
                  </a>
                  <small>
                    <a href="#">View Store Coupons</a>
                  </small>
                </div>

                <div className="coupon-meta">
                  <h5 className="theme_color">
                    <a href="#">50% Discount Coupon</a>
                  </h5>
                  <p>
                    At reasonable prices, quality assurance, 100% secure
                    shopping.
                  </p>
                </div>

                <div className="showcode">
                  <a href="#" className="code-link">
                    <span
                      className="coupon-code"
                      data-original-title=""
                      title=""
                    />
                    <span className="show-code print-code">
                      <i className="fa fa-eye" /> View More
                    </span>
                  </a>
                </div>

                <div className="coupon-bottom clearfix">
                  <small className="pull-left">Expire : 21/07/2016</small>
                </div>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="coupon-list list-wrapper">
              <div className="coupon-wrapper">
                <div className="post-media text-center">
                  <a href="#">
                    <img
                      src="images/promo_01.png"
                      alt=""
                      className="img-responsive"
                    />
                  </a>
                  <small>
                    <a href="#">View Store Coupons</a>
                  </small>
                </div>

                <div className="coupon-meta">
                  <h5 className="theme_color">
                    <a href="#">50% Discount Coupon</a>
                  </h5>
                  <p>
                    At reasonable prices, quality assurance, 100% secure
                    shopping.
                  </p>
                </div>

                <div className="showcode">
                  <a href="#" className="code-link">
                    <span
                      className="coupon-code"
                      data-original-title=""
                      title=""
                    />
                    <span className="show-code print-code">
                      <i className="fa fa-eye" /> View More
                    </span>
                  </a>
                </div>

                <div className="coupon-bottom clearfix">
                  <small className="pull-left">Expire : 21/07/2016</small>
                </div>
              </div>
            </div>
          </div>
        </div>

        <h3 className="theme_color">Similar Competition</h3>
        <div id="carousel2" className="owl-carousel">
          <div className="item">
            <div className="coupon-list list-wrapper">
              <div className="coupon-wrapper">
                <div className="post-media text-center">
                  <a href="#">
                    <img
                      src="images/promo_01.png"
                      alt=""
                      className="img-responsive"
                    />
                  </a>
                  <small>
                    <a href="#">View Store Coupons</a>
                  </small>
                </div>

                <div className="coupon-meta">
                  <h5 className="theme_color">
                    <a href="#">50% Discount Coupon</a>
                  </h5>
                  <p>
                    At reasonable prices, quality assurance, 100% secure
                    shopping.
                  </p>
                </div>

                <div className="showcode">
                  <a href="#" className="code-link">
                    <span
                      className="coupon-code"
                      data-original-title=""
                      title=""
                    />
                    <span className="show-code print-code">
                      <i className="fa fa-eye" /> View More
                    </span>
                  </a>
                </div>

                <div className="coupon-bottom clearfix">
                  <small className="pull-left">Expire : 21/07/2016</small>
                </div>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="coupon-list list-wrapper">
              <div className="coupon-wrapper">
                <div className="post-media text-center">
                  <a href="#">
                    <img
                      src="images/promo_01.png"
                      alt=""
                      className="img-responsive"
                    />
                  </a>
                  <small>
                    <a href="#">View Store Coupons</a>
                  </small>
                </div>

                <div className="coupon-meta">
                  <h5 className="theme_color">
                    <a href="#">50% Discount Coupon</a>
                  </h5>
                  <p>
                    At reasonable prices, quality assurance, 100% secure
                    shopping.
                  </p>
                </div>

                <div className="showcode">
                  <a href="#" className="code-link">
                    <span
                      className="coupon-code"
                      data-original-title=""
                      title=""
                    />
                    <span className="show-code print-code">
                      <i className="fa fa-eye" /> View More
                    </span>
                  </a>
                </div>

                <div className="coupon-bottom clearfix">
                  <small className="pull-left">Expire : 21/07/2016</small>
                </div>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="coupon-list list-wrapper">
              <div className="coupon-wrapper">
                <div className="post-media text-center">
                  <a href="#">
                    <img
                      src="images/promo_01.png"
                      alt=""
                      className="img-responsive"
                    />
                  </a>
                  <small>
                    <a href="#">View Store Coupons</a>
                  </small>
                </div>

                <div className="coupon-meta">
                  <h5 className="theme_color">
                    <a href="#">50% Discount Coupon</a>
                  </h5>
                  <p>
                    At reasonable prices, quality assurance, 100% secure
                    shopping.
                  </p>
                </div>

                <div className="showcode">
                  <a href="#" className="code-link">
                    <span
                      className="coupon-code"
                      data-original-title=""
                      title=""
                    />
                    <span className="show-code print-code">
                      <i className="fa fa-eye" /> View More
                    </span>
                  </a>
                </div>

                <div className="coupon-bottom clearfix">
                  <small className="pull-left">Expire : 21/07/2016</small>
                </div>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="coupon-list list-wrapper">
              <div className="coupon-wrapper">
                <div className="post-media text-center">
                  <a href="#">
                    <img
                      src="images/promo_01.png"
                      alt=""
                      className="img-responsive"
                    />
                  </a>
                  <small>
                    <a href="#">View Store Coupons</a>
                  </small>
                </div>

                <div className="coupon-meta">
                  <h5 className="theme_color">
                    <a href="#">50% Discount Coupon</a>
                  </h5>
                  <p>
                    At reasonable prices, quality assurance, 100% secure
                    shopping.
                  </p>
                </div>

                <div className="showcode">
                  <a href="#" className="code-link">
                    <span
                      className="coupon-code"
                      data-original-title=""
                      title=""
                    />
                    <span className="show-code print-code">
                      <i className="fa fa-eye" /> View More
                    </span>
                  </a>
                </div>

                <div className="coupon-bottom clearfix">
                  <small className="pull-left">Expire : 21/07/2016</small>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );

  render() {
    return (
      <Fragment>
        <div
          className="parallax first-section"
          data-stellar-background-ratio="0.5"
        >
          <div className="container">
            <div className="section-title m30 text-center">
              <h1>User Dashboard</h1>
              <p>
                In dignissim feugiat gravida. Proin feugiat quam sed gravida
                fringilla. Proin quis mauris ut magna fringilla vulputate quis
                non ante. Integer bibendum velit dui. Sed consequat nisi id
                convallis eleifend.{" "}
              </p>
            </div>
          </div>
        </div>

        <div className="section wb dash_wrap">
          <div className="container">
            <div className="row">
              <div className="sidebar col-md-4">
                <div className="widget clearfix">
                  <ul className="nav nav-pills nav-stacked">
                    <li className="active">
                      <a
                        onClick={this.handleSideNav}
                        name="Dashboard"
                        id="v-pills-home-tab"
                        data-toggle="pill"
                        href="#v-pills-home"
                        role="tab"
                        aria-controls="v-pills-home"
                        aria-selected="true"
                      >
                        <span className="fa fa-home" /> Dashboard
                      </a>
                    </li>
                    <li>
                      <a
                        onClick={this.handleSideNav}
                        name="Profile"
                        id="v-pills-home-tab1"
                        data-toggle="pill"
                        href="#v-pills-home1"
                        role="tab"
                        aria-controls="v-pills-home1"
                        aria-selected="true"
                      >
                        <span className="fa fa-star" /> My Profile
                      </a>
                    </li>
                    <li>
                      <a
                        onClick={this.handleSideNav}
                        name="Competition"
                        id="v-pills-home-tab2"
                        data-toggle="pill"
                        href="#v-pills-home2"
                        role="tab"
                        aria-controls="v-pills-home2"
                        aria-selected="true"
                      >
                        <span className="fa fa-heart-o" /> My Competetions
                      </a>
                    </li>
                    <li>
                      <a
                        id="v-pills-home-tab4"
                        onClick={this.handleLogout}
                        data-toggle="pill"
                        href="#"
                      >
                        <span className="fa fa-lock" /> Logout
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <div className="cntn_area col-md-8">
                <div className="tab-content" id="v-pills-tabContent">
                  {this.state.activeLink === 'Dashboard' && this.renderDashBoard()}
                  {this.state.activeLink === 'Profile' && <Profile user={this.props.user} />}
                  {this.state.activeLink === 'Competition' && <Competition />}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default DashboardProfile;
