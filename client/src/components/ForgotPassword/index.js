import React,{ Component } from 'react';
class ForgotPassword extends Component {
  constructor(props){
    super(props);
    this.state = {}
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.forgotPasswordRequest(this.state);
  }

  render() {
    return (
      <div
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="login-reg modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="form-container">
              <div className="img-container">
                <img src="images/signup.png" alt="" />
              </div>
              <div className="form-section">
                <h2>Forgot Password</h2>
                <form onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Email address</label>
                    <input
                      type="email"
                      name="email"
                      onChange={this.handleChange}
                      className="form-control"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                      placeholder="Enter your registered email address"
                      value={this.state.email}
                    />
                  </div>
                  
                  <button type="submit" className="btn btn-primary">
                    Send
                  </button>
                </form>
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </div>
      
    );
  }
}

export default ForgotPassword;