import React, { Component } from "react";

class HomeFeeds extends Component {
  render() {
    return (
      <div className="section wb">
        <div className="container">
          <div className="text-center site_heading">
            <h3>Get Coupon By gaming type</h3>
          </div>
          <div className="row">
            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="promobox wow fadeInLeft" data-wow-delay="1s">
                <div className="post-media">
                  <a href="#">
                    <img
                      src="images/promo_01.png"
                      alt=""
                      className="img-responsive"
                    />
                  </a>
                  <div className="promo-desc">
                    <img
                      src="images/small_01.png"
                      alt=""
                      className="alignleft img-thumbnail"
                    />
                    <h4>
                      <a href="#">Get coupon for thrilling games</a>
                    </h4>
                    <small>128 Available Coupons</small>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="promobox wow fadeInUp" data-wow-delay="1s">
                <div className="post-media">
                  <a href="#">
                    <img
                      src="images/promo_02.png"
                      alt=""
                      className="img-responsive"
                    />
                  </a>

                  <div className="promo-desc">
                    <img
                      src="images/small_02.png"
                      alt=""
                      className="alignleft img-thumbnail"
                    />
                    <h4>
                      <a href="#">Get coupon for adventure games</a>
                    </h4>
                    <small>44 Available Coupons</small>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-md-4 col-sm-4 col-xs-12">
              <div className="promobox wow fadeInRight" data-wow-delay="1s">
                <div className="post-media">
                  <a href="#">
                    <img
                      src="images/promo_01.png"
                      alt=""
                      className="img-responsive"
                    />
                  </a>

                  <div className="promo-desc">
                    <img
                      src="images/small_03.png"
                      alt=""
                      className="alignleft img-thumbnail"
                    />
                    <h4>
                      <a href="#">Get coupon for thrilling games</a>
                    </h4>
                    <small>55 Available Coupons</small>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <section className="mid_sec">
            <div className="row">
              <div className="content col-md-8">
                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/str1.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">
                              50% Discount Coupon from CatiLogoms.com
                            </a>
                          </h3>
                          <p>
                            At reasonable prices, quality assurance, 100% secure
                            shopping. Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiolore consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span className="coupon-code">2016TATILRA50</span>
                            <span className="show-code">Show Code</span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">
                            Expire : 21/07/2016
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/store_13.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">Greensoil Printable Coupon Code</a>
                          </h3>
                          <p>
                            100% secure shopping at reasonable prices, quality
                            assurance. Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiolore consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span className="coupon-code">PRINT ME</span>
                            <span className="show-code print-code">
                              <i className="fa fa-print" /> Print Coupon
                            </span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">
                            Expire : 21/07/2016
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/str2.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">MyLogo.com 10$ off for all orders</a>
                          </h3>
                          <p>
                            Win tablet pc discount on your MyLogo com shopping
                            plus free... Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiolore consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span className="coupon-code">2016TATILRA33</span>
                            <span className="show-code">Show Code</span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">
                            Expire : 21/07/2016
                          </small>
                          <small className="pull-right">
                            <a href="#">
                              <i className="fa fa-comment-o" /> 5 Comments
                            </a>
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/store_12.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">
                              Follow Skinuque on Facebook get 10% Off
                            </a>
                          </h3>
                          <p>
                            Follow Skinuque on Facebook get 10% Off 100% secure
                            shopping at reasonable prices, quality assurance.
                            Lorem ipsum dolor sit amet, consectetur adipisiciur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span className="coupon-code">PRINT ME</span>
                            <span className="show-code print-code">
                              <i className="fa fa-search" /> View Deal
                            </span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">
                            Expire : 21/07/2016
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/str3.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">44$ off CompanieNamis Discount</a>
                          </h3>
                          <p>
                            Bringing a new breath to the fashion
                            CompanieNamis's.. Lorem ipsum dolor sit amet,
                            consectetur adipisicing elit, sed do eiolore
                            consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span className="coupon-code">2016TATILRA56</span>
                            <span className="show-code">Show Code</span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">
                            Expire : 21/07/2016
                          </small>
                          <small className="pull-right">
                            <a href="#">
                              <i className="fa fa-comment-o" /> 21 Comments
                            </a>
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/str5.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">
                              10% Discount Coupon from KnowLogoDesign
                            </a>
                          </h3>
                          <p>
                            Those new members to the site via our instant
                            KnowLogoDesign.com.. Lorem ipsum dolor sit amet,
                            consectetur adipisicing elit, sed do eiolore
                            consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span className="coupon-code">2016TATILRA51</span>
                            <span className="show-code">Show Code</span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">
                            Expire : 21/07/2016
                          </small>
                          <small className="pull-right">
                            <a href="#">
                              <i className="fa fa-comment-o" /> 51 Comments
                            </a>
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/str4.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">Free Shipping for All Orders</a>
                          </h3>
                          <p>
                            Get free shipping for all your next orders from this
                            store.. Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiolore consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span className="coupon-code">2016TATILRA29</span>
                            <span className="show-code">Show Code</span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">
                            Expire : 21/07/2016
                          </small>
                          <small className="pull-right">
                            <a href="#">
                              <i className="fa fa-comment-o" /> 0 Comment
                            </a>
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div className="col-md-4 col-sm-4 col-xs-12">
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/str1.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">$5 for for your next logo design</a>
                          </h3>
                          <p>
                            Get free shipping for all your next orders from this
                            store.. Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiolore consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span className="coupon-code">2016TATILRAAA</span>
                            <span className="show-code">Show Code</span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">
                            Expire : 21/07/2016
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <nav className="nav-pagi">
                  <ul className="pagination">
                    <li>
                      <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">1</a>
                    </li>
                    <li>
                      <a href="#">2</a>
                    </li>
                    <li>
                      <a href="#">3</a>
                    </li>
                    <li>
                      <a href="#">4</a>
                    </li>
                    <li>
                      <a href="#">5</a>
                    </li>
                    <li>
                      <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>

              <div className="sidebar col-md-4 col-sm-12 wow fadeInRight">
                <div className="widget custom-widget clearfix">
                  <a href="#">
                    <i className="fa fa-bullhorn alignleft fa-3x" />
                    <h4>Submit a Coupon</h4>
                    <p>Share your code discount's everyone</p>
                  </a>
                </div>

                <div className="widget clearfix">
                  <div className="widget-title">
                    <h4>
                      <span>Best Coupons</span>
                    </h4>
                  </div>

                  <div className="best-coupons">
                    <ul className="customlist">
                      <li>
                        <a href="#">MyLogo.com 10$ off for all orders</a>
                      </li>
                      <li>
                        <a href="#">44$ off CompanieNamis Discount</a>
                      </li>
                      <li>
                        <a href="#">10% Discount Coupon from Gosi</a>
                      </li>
                      <li>
                        <a href="#">Free Shipping for All Orders</a>
                      </li>
                      <li>
                        <a href="#">$5 for for your next logo design</a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className="widget clearfix">
                  <div className="featured hidden-xs">
                    <i className="fa fa-star-o" />
                  </div>
                  <div className="widget-title">
                    <h4>
                      <span>Best Stores</span>
                    </h4>
                  </div>

                  <div className="text-center store-list row">
                    <div className="col-md-6 col-xs-6">
                      <div className="post-media">
                        <a href="#">
                          <img
                            src="images/side.jpg"
                            alt=""
                            className="img-responsive"
                          />
                        </a>
                      </div>
                    </div>
                    <div className="col-md-6 col-xs-6">
                      <div className="post-media">
                        <a href="#">
                          <img
                            src="images/side2.jpg"
                            alt=""
                            className="img-responsive"
                          />
                        </a>
                      </div>
                    </div>

                    <div className="col-md-6 col-xs-6">
                      <div className="post-media">
                        <a href="#">
                          <img
                            src="images/side3.jpg"
                            alt=""
                            className="img-responsive"
                          />
                        </a>
                      </div>
                    </div>

                    <div className="col-md-6 col-xs-6">
                      <div className="post-media">
                        <a href="#">
                          <img
                            src="images/side.jpg"
                            alt=""
                            className="img-responsive"
                          />
                        </a>
                      </div>
                    </div>

                    <div className="col-md-6 col-xs-6">
                      <div className="post-media">
                        <a href="#">
                          <img
                            src="images/side3.jpg"
                            alt=""
                            className="img-responsive"
                          />
                        </a>
                      </div>
                    </div>

                    <div className="col-md-6 col-xs-6">
                      <div className="post-media">
                        <a href="#">
                          <img
                            src="images/side2.jpg"
                            alt=""
                            className="img-responsive"
                          />
                        </a>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="widget clearfix">
                  <div className="widget-title">
                    <h4>
                      <span>Email Newsletter</span>
                    </h4>
                  </div>

                  <div className="newsletter">
                    <p>
                      Your email is safe with us and we hate spam as much as you
                      do. Lorem ipsum dolor sit amet et dolore.
                    </p>
                    <form className="">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Enter your name.."
                      />
                      <input
                        type="email"
                        className="form-control"
                        placeholder="Enter your email.."
                      />
                      <button type="submit" className="btn btn-primary">
                        Subscribe
                      </button>
                    </form>
                  </div>
                </div>
                <img src="images/ad_sid.jpg" />
              </div>
            </div>
          </section>

          <div className="footer-content">
            <div className="row">
              <div className="col-md-12">
                <div className="site_heading">
                  <h3>Popular Categories</h3>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-md-2 col-sm-2 col-xs-12">
                <ul className="customlist">
                  <li>
                    <a href="#">Baby Kids</a>
                  </li>
                  <li>
                    <a href="#">Books</a>
                  </li>
                  <li>
                    <a href="#">Computers</a>
                  </li>
                  <li>
                    <a href="#">Cameras</a>
                  </li>
                  <li>
                    <a href="#">Electronics</a>
                  </li>
                </ul>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12">
                <ul className="customlist">
                  <li>
                    <a href="#">Games</a>
                  </li>
                  <li>
                    <a href="#">Gifts</a>
                  </li>
                  <li>
                    <a href="#">Health Beauty</a>
                  </li>
                  <li>
                    <a href="#">Home Garden</a>
                  </li>
                  <li>
                    <a href="#">Home Supplies</a>
                  </li>
                </ul>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12">
                <ul className="customlist">
                  <li>
                    <a href="#">Laptops</a>
                  </li>
                  <li>
                    <a href="#">Entertainetmen</a>
                  </li>
                  <li>
                    <a href="#">Digital Stores</a>
                  </li>
                  <li>
                    <a href="#">Marketplaces</a>
                  </li>
                  <li>
                    <a href="#">Musicians</a>
                  </li>
                </ul>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12">
                <ul className="customlist">
                  <li>
                    <a href="#">Movies & Films</a>
                  </li>
                  <li>
                    <a href="#">Phones</a>
                  </li>
                  <li>
                    <a href="#">Travel</a>
                  </li>
                  <li>
                    <a href="#">Televisions</a>
                  </li>
                  <li>
                    <a href="#">Telegraphers</a>
                  </li>
                </ul>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12">
                <ul className="customlist">
                  <li>
                    <a href="#">Baby Toys</a>
                  </li>
                  <li>
                    <a href="#">Clothings</a>
                  </li>
                  <li>
                    <a href="#">Jewellry</a>
                  </li>
                  <li>
                    <a href="#">Car Supplies</a>
                  </li>
                  <li>
                    <a href="#">Watches</a>
                  </li>
                </ul>
              </div>

              <div className="col-md-2 col-sm-2 col-xs-12">
                <ul className="customlist">
                  <li>
                    <a href="#">Glassess</a>
                  </li>
                  <li>
                    <a href="#">Medical</a>
                  </li>
                  <li>
                    <a href="#">Pet Shops</a>
                  </li>
                  <li>
                    <a href="#">LifeStyle</a>
                  </li>
                  <li>
                    <a href="#">Sports</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HomeFeeds
