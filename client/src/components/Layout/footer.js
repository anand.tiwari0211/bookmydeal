import React, { Fragment } from "react";
import { Link } from "react-router-dom";

const Footer = () => (
  <Fragment>
    <footer className="footer">
      <div className="container">
        <div className="row">
          <div className="col-md-3 col-sm-12 col-xs-12">
            <div className="widget clearfix">
              <div className="widget-title">
                <h4>Email Newsletter</h4>
              </div>

              <div className="newsletter">
                <p>
                  Your email is safe with us and we hate spam as much as you do.
                  Lorem ipsum dolor sit amet et dolore.
                </p>

                <form className="">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Enter your name.."
                  />

                  <input
                    type="email"
                    className="form-control"
                    placeholder="Enter your email.."
                  />

                  <button type="submit" className="btn btn-primary">
                    SUBSCRIBE
                  </button>
                </form>
              </div>
            </div>
          </div>

          <div className="col-md-3 col-sm-12 col-xs-12">
            <div className="widget clearfix">
              <div className="widget-title">
                <h4>Useful Links</h4>
              </div>

              <ul className="footer-links">
                <li>
                  <a href="#">Help</a>
                </li>

                <li>
                  <a href="#">Sitemap</a>
                </li>

                <li>
                  <a href="#">Press</a>
                </li>

                <li>
                  <a href="#">Career</a>
                </li>

                <li>
                  <a href="#">Coupon & listing</a>
                </li>

                <li>
                  <a href="#">License</a>
                </li>

                <li>
                  <a href="#">Trademarks</a>
                </li>
              </ul>
            </div>
          </div>

          <div className="col-md-2 col-sm-12 col-xs-12">
            <div className="widget clearfix">
              <div className="widget-title">
                <h4>Company Info</h4>
              </div>

              <ul className="footer-links">
                <li>
                  <Link to="/competetion">Search Competition</Link>
                </li>

                <li>
                  <Link to="/contact">Contact us</Link>
                </li>

                <li>
                  <a href="#">Account Settings</a>
                </li>

                <li>
                  <a href="#">Copyrights</a>
                </li>

                <li>
                  <Link to="/coupon">Coupon</Link>
                </li>

                <li>
                  <a href="#">License</a>
                </li>

                <li>
                  <a href="#">Trademarks</a>
                </li>
              </ul>
            </div>
          </div>

          <div className="col-md-4 col-sm-12 col-xs-12">
            <div className="widget clearfix">
              <div className="widget-title">
                <h4>Frequently Asked Questions</h4>
              </div>

              <div
                className="first_accordion withicon withcolorful panel-group"
                id="accordion5"
              >
                <div className="panel panel-primary">
                  <div className="panel-heading">
                    <h4 className="panel-title">
                      <a
                        data-toggle="collapse"
                        data-parent="#accordion5"
                        href="#collapse115"
                      >
                        <i className="fa fa-angle-down" /> How to Use Coupon Codes?
                      </a>
                    </h4>
                  </div>

                  <div id="collapse115" className="panel-collapse collapse in">
                    <div className="panel-body">
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiolore. consectetur adipisicing elit, sed
                        do eiusmod tempor ut labore et dolor.
                      </p>
                    </div>
                  </div>
                </div>

                <div className="panel panel-primary">
                  <div className="panel-heading">
                    <h4 className="panel-title">
                      <a
                        data-toggle="collapse"
                        data-parent="#accordion5"
                        href="#collapse215"
                      >
                        <i className="fa fa-angle-down" /> Can I submit My Coupons?
                      </a>
                    </h4>
                  </div>

                  <div id="collapse215" className="panel-collapse collapse">
                    <div className="panel-body">
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiolore. consectetur adipisicing elit, sed
                        do eiusmod tempor ut labore et dolor.
                      </p>
                    </div>
                  </div>
                </div>

                <div className="panel panel-primary last">
                  <div className="panel-heading">
                    <h4 className="panel-title">
                      <a
                        data-toggle="collapse"
                        data-parent="#accordion5"
                        href="#collapse315"
                      >
                        <i className="fa fa-angle-down" /> Coupon Submission free?
                      </a>
                    </h4>
                  </div>

                  <div id="collapse315" className="panel-collapse collapse">
                    <div className="panel-body">
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiolore. consectetur adipisicing elit, sed
                        do eiusmod tempor ut labore et dolor.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <div className="copyrights">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-sm-6">
            <div className="copylinks">
              <p>BookMyDeal &copy; 2019 | All Rights Reserved</p>
            </div>
          </div>

          <div className="col-md-6 col-sm-6">
            <div className="footer-social text-right">
              <ul className="list-inline social-small">
                <li>
                  <a href="#">
                    <i className="fa fa-facebook" />
                  </a>
                </li>

                <li>
                  <a href="#">
                    <i className="fa fa-twitter" />
                  </a>
                </li>

                <li>
                  <a href="#">
                    <i className="fa fa-google-plus" />
                  </a>
                </li>

                <li>
                  <a href="#">
                    <i className="fa fa-linkedin" />
                  </a>
                </li>

                <li>
                  <a href="#">
                    <i className="fa fa-pinterest" />
                  </a>
                </li>

                <li>
                  <a href="#">
                    <i className="fa fa-rss" />
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </Fragment>
)

export default Footer
