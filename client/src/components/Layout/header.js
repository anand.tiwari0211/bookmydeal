import React, { Fragment, Component } from "react";
import { Link } from "react-router-dom";

class Header extends Component {
  componentDidMount() {
    if (this.props.isLoggedIn) {
      this.props.getUserRequest();
    }
  }
  render() {
    return (
      <header>
        <div id="wrapper">
          <div className="header">
            <div className="topbar">
              <div className="container">
                <div className="row">
                  <div className="col-lg-2 col-md-3 col-sm-4 col-xs-4">
                    <Link to="/">
                      <img src="images/logo.png" className="logo_img" />
                    </Link>
                  </div>
                  <div className="col-lg-10 col-md-9 col-sm-8 col-xs-8">
                    <form
                      className="form-inline form_custm pull-right"
                      action="#"
                    >
                      <div className="form-group">
                        <span className="fa fa-search plchdl_srch" />
                        <input
                          type="text"
                          className="form-control"
                          id="search"
                          placeholder="Search stores for coupen, deals..."
                          name="search"
                        />
                      </div>
                      <div className="navbar-collapse collapse login-navigation">
                        <ul className="nav navbar-nav navbar-right login-nav">
                          {!this.props.isLoggedIn && (
                            <Fragment>
                              <li className="sign_frm">
                                <a
                                  href="#"
                                  type="button"
                                  className="wow fadeInUp"
                                  data-wow-duration="1s"
                                  data-wow-delay="0.4s"
                                  onClick={() =>
                                    this.props.doModalOpen({
                                      modalIsOpen: true,
                                      type: "Signup"
                                    })
                                  }
                                >
                                  <span className="fa fa-user" /> Sign Up
                                </a>
                              </li>
                              <li className="sign_frm">
                                <a
                                  href="#"
                                  type="button"
                                  className="wow fadeInUp"
                                  data-wow-duration="1s"
                                  data-wow-delay="0.5s"
                                  onClick={() =>
                                    this.props.doModalOpen({
                                      modalIsOpen: true,
                                      type: "Login"
                                    })
                                  }
                                >
                                  <span className="fa fa-power-off" /> Sign In
                                </a>
                              </li>
                            </Fragment>
                          )}
                          {this.props.isLoggedIn && (
                            <li className="dropdown hasmenu userpanel prof_usr">
                              <img
                                src={
                                  this.props.user.profilePicture ||
                                  "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png"
                                }
                                alt=""
                                className="img-circle"
                              />
                              &nbsp;&nbsp;
                              {this.props.user && this.props.user.name && (
                                <small>
                                  Hi, {this.props.user.name.substring(0, 10)}...
                                </small>
                              )}
                              &nbsp;&nbsp;
                              <span className="fa fa-angle-down" />
                              <ul
                                className="dropdown-menu start-right"
                                role="menu"
                              >
                                <li>
                                  <Link to="/dashboard">
                                    <i className="fa fa-dashboard" /> Dashboard
                                  </Link>
                                </li>
                                <li>
                                  <Link to="/dashboard">
                                    <i className="fa fa-star" /> Profile
                                  </Link>
                                </li>
                                <li>
                                  <Link
                                    onClick={() => this.props.onLogoutRequest()}
                                  >
                                    <i className="fa fa-lock" /> Logout
                                  </Link>
                                </li>
                              </ul>
                            </li>
                          )}
                        </ul>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <div className="menu-wrapper ab-menu" id="header_stick">
              <div className="container">
                <div className="col-lg-offset-2 col-md-offset-3">
                  <div className="hovermenu ttmenu menu-color">
                    <div className="navbar navbar-default" role="navigation">
                      <div className="navbar-header">
                        <button
                          type="button"
                          className="navbar-toggle collapsed"
                          data-toggle="collapse"
                          data-target=".navbar-collapse"
                        >
                          <span className="sr-only">Toggle navigation</span>
                          <span className="icon-bar" />
                          <span className="icon-bar" />
                          <span className="icon-bar" />
                        </button>
                      </div>
                      <div className="logo_sticky">
                        <a href="#">
                          <img
                            src="images/sticky_logo.png"
                            className="logo_img"
                          />
                        </a>
                      </div>
                      <div className="navbar-collapse collapse">
                        <ul className="nav navbar-nav">
                          <li>
                            <Link
                              to="/competetion"
                              title=""
                              className="wow fadeInUp"
                              data-wow-duration="1s"
                              data-wow-delay="0.1s"
                            >
                              Competetions
                            </Link>
                          </li>
                          <li>
                            <Link
                              to="/store"
                              title=""
                              className="wow fadeInUp"
                              data-wow-duration="1s"
                              data-wow-delay="0.2s"
                            >
                              Stores
                            </Link>
                          </li>
                          <li>
                            <Link
                              to="/coupon"
                              title=""
                              className="wow fadeInUp"
                              data-wow-duration="1s"
                              data-wow-delay="0.3s"
                            >
                              Coupons
                            </Link>
                          </li>
                          {!this.props.isLoggedIn && (
                      <li className="sign-in-up" style={{display:"none"}}>
                        <a
                              href="#"
                              type="button"
                              className="wow fadeInUp"
                              data-wow-duration="1s"
                              data-wow-delay="0.5s"
                              onClick={() =>
                                this.props.doModalOpen({
                                  modalIsOpen: true,
                                  type: "Login"
                                })
                              }
                            >
                              Sign In
                            </a>
                      </li>
                      )}
                      {!this.props.isLoggedIn && (
                      <li  className="sign-in-up" style={{display:"none"}}>

                        <a
                              href="#"
                              type="button"
                              className="wow fadeInUp"
                              data-wow-duration="1s"
                              data-wow-delay="0.4s"
                              onClick={() =>
                                this.props.doModalOpen({
                                  modalIsOpen: true,
                                  type: "Signup"
                                })
                              }
                            >
                              Sign Up
                            </a>
                      </li>
                      )}
                          {this.props.isLoggedIn && (
                      <li className="sign-in-up" style={{display:"none"}}>
                            <Link to='/dashboard'>
                              Dashboard
                            </Link>
                          </li>
                          )}
                      {this.props.isLoggedIn && (
                          <li className="sign-in-up" style={{display:"none"}}>
                            <Link to='/dashboard'>
                              My Profile
                            </Link>
                          </li>
                          )}
                      {this.props.isLoggedIn && (
                          <li className="sign-in-up" style={{display:"none"}}>
                            <Link onClick={() => this.props.onLogoutRequest()}>
                              Logout
                            </Link>
                          </li>
                          )}
                        </ul>

                        {/* <Link
                    to="/cart" className="wager_list">
                    <span className="fa fa-shopping-cart" />
                  </Link>*/}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
