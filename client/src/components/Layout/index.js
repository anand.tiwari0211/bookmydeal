import React, { Component, Fragment } from "react";
import Header from "../../containers/Layout/header";
import Footer from "./footer";

class Layout extends Component {
  render() {
    return (
      <Fragment>
        <Header />
        {this.props.children}
        <Footer />
      </Fragment>
    )
  }
}

export default Layout
