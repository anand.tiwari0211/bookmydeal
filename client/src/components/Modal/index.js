import React,{ Component } from 'react';
import Login from '../../containers/Login';
import Signup from '../../containers/Signup';
import ResetPassword from '../../containers/ForgotPassword/ResetPassword';
import ForgotPassword from '../../containers/ForgotPassword'
import Modal from 'react-modal';
 
const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    transform             : 'translate(-50%, -50%)',
    marginRight           : '-20%',
    border                : 'none',
    background            : 'none'
  }
};
 
// Make sure to bind modal to your appElement (http://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('#root')
 
class ReactModal extends Component {

  componentDidMount() {
    if(!!window.localStorage.getItem('linktoken')) {
      this.props.doModalOpen({ modalIsOpen: true, type: 'Reset-Password' });
    }
  }

  closeModal = () => {
    this.props.doModalClose({ modalIsOpen: false, type: '' });
  }

  renderModalContent = () => {
    const { type } = this.props;
    switch(type) {
      case 'Login':
        return <Login />
      case 'Signup':
        return <Signup />
      case 'Reset-Password':
        return <ResetPassword />
      case 'Forgot-Password':
        return <ForgotPassword />
      default:
        return null
    }
  }

  render() {
    return (
      <Modal
        isOpen={this.props.modalIsOpen}
        style={customStyles}
      >
        <a onClick={this.closeModal} className="closeIcon"><img src="images/closeIcon.png"  /></a>
        {this.renderModalContent()}
      </Modal>
    );
  }
}

export default ReactModal;