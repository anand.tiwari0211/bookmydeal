import React from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Notification = (props) => (
  <div>
    <ToastContainer autoClose={props.autoClose}/>
  </div>
);

export default Notification;