import React, { Component } from "react";

class PopupStore extends Component {
  render() {
    return (
      <section className="popu_stores">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="site_heading">
                <h3>Popular Stores</h3>
              </div>
            </div>
          </div>
          <div className="text-center store-list row">
            <div
              className="col-md-2 col-sm-4 col-xs-6 wow fadeInUp"
              data-wow-duration="1s"
              data-wow-delay="0.2s"
            >
              <div className="post-media">
                <a href="#">
                  <img
                    src="images/store_01.jpg"
                    alt=""
                    className="img-responsive"
                  />
                </a>
                <small>Takifest.com</small>
              </div>
            </div>
            <div
              className="col-md-2 col-sm-4 col-xs-6 wow fadeInUp"
              data-wow-duration="1s"
              data-wow-delay="0.3s"
            >
              <div className="post-media">
                <a href="#">
                  <img
                    src="images/store_02.jpg"
                    alt=""
                    className="img-responsive"
                  />
                </a>
                <small>WPServis.com</small>
              </div>
            </div>

            <div
              className="col-md-2 col-sm-4 col-xs-6 wow fadeInUp"
              data-wow-duration="1s"
              data-wow-delay="0.4s"
            >
              <div className="post-media">
                <a href="#">
                  <img
                    src="images/store_03.jpg"
                    alt=""
                    className="img-responsive"
                  />
                </a>
                <small>PurplebyBanu.com</small>
              </div>
            </div>

            <div
              className="col-md-2 col-sm-4 col-xs-6 wow fadeInUp"
              data-wow-duration="1s"
              data-wow-delay="0.5s"
            >
              <div className="post-media">
                <a href="#">
                  <img
                    src="images/store_04.jpg"
                    alt=""
                    className="img-responsive"
                  />
                </a>
                <small>Tutsplus.com</small>
              </div>
            </div>

            <div
              className="col-md-2 col-sm-4 col-xs-6 wow fadeInUp"
              data-wow-duration="1s"
              data-wow-delay="0.6s"
            >
              <div className="post-media">
                <a href="#">
                  <img
                    src="images/store_05.jpg"
                    alt=""
                    className="img-responsive"
                  />
                </a>
                <small>Showwp.com</small>
              </div>
            </div>

            <div
              className="col-md-2 col-sm-4 col-xs-6 wow fadeInUp"
              data-wow-duration="1s"
              data-wow-delay="0.7s"
            >
              <div className="post-media">
                <a href="#">
                  <img
                    src="images/store_06.jpg"
                    alt=""
                    className="img-responsive"
                  />
                </a>
                <small>PSDConvertHTML.com</small>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default PopupStore
