import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";

class SearchCompetetion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      competitions: []
    }
  }

  componentDidMount() {
    this.props.getCompetitionsRequest()
  }

  componentDidUpdate(prevProps) {
    if(prevProps.timestamp !== this.props.timestamp) {
      this.setState({ competitions: this.props.competitions });
    }
  }

  handleSearch = e => {
    const competitions = [...this.props.competitions];
    const filteredCompetitions = e.target.value
    ? competitions.filter(comp => comp.title.toLowerCase().startsWith(e.target.value.toLowerCase()))
    : competitions
    this.setState({ competitions: filteredCompetitions });
  }

  render() {
    const competitions = this.state.competitions;
    return (
      <Fragment>
        <div
          className="parallax first-section"
          data-stellar-background-ratio="0.5"
        >
          <div className="container">
            <div className="section-title m30 text-center">
              <h1>Competetions</h1>
            </div>
          </div>
        </div>

        <div className="section wb">
          <div className="container">
            <div className="row">
             <div className="col-md-3 col-sm-12">
             <div  class="filetr-widget slideUp genreFilter" >
                 <div class="widget-title1">
                   <h3>Categories</h3>
                </div>
               <a  class="clear-filters">Clear</a>
                 <div id="genreFilter" class="filter-list">
                   <ul id="childGenre" class="__filter-list">
                      <li class="__filter"><input name="subcollection" type="radio" value="Simulation" /> <span class="checkbox-label">Simulation</span></li>
                      <li class="__filter"><input name="subcollection" type="radio" value="Bowling" /> <span class="checkbox-label">In-mall Gaming Zone</span></li>
                      <li class="__filter"><input name="subcollection" type="radio" value="Bowling" /> <span class="checkbox-label">Vr Gaming</span></li>
                     </ul>
                 </div>
              </div>
              <div class="filetr-widget slideUp genreFilter" >
                 <div class="widget-title1" >
                   <h3>Date</h3>
                </div>
               <a class="clear-filters">Clear</a>
                 <div id="genreFilter" class="filter-list">
                   <ul id="childGenre" class="__filter-list">
                      <li class="__filter"><input name="subcollection" type="radio" value="Simulation" /> <span class="checkbox-label">Today</span></li>
                      <li class="__filter"><input name="subcollection" type="radio" value="Bowling" /> <span class="checkbox-label">Tomorrow</span></li>
                      <li class="__filter"><input name="subcollection" type="radio" value="Bowling" /> <span class="checkbox-label">Weekend</span></li>
                     </ul>
                 </div>
              </div>
              <div  class="filetr-widget slideUp genreFilter" >
                 <div class="widget-title1" >
                   <h3>Price Range</h3>
                </div>
               <a  class="clear-filters">Clear</a>
                 <div id="genreFilter" class="filter-list">
                   <ul id="childGenre" class="__filter-list">
                     <li class="__filter"><input name="subcollection" type="radio" value="Simulation" /> <span class="checkbox-label">0 - 500</span></li>
                      <li class="__filter"><input name="subcollection" type="radio" value="Bowling" /> <span class="checkbox-label">501 - 2000</span></li>
                      <li class="__filter"><input name="subcollection" type="radio" value="Bowling" /> <span class="checkbox-label">2001 and above</span></li>
                     </ul>
                 </div>
              </div>
             </div>
             <div className="col-md-9 col-sm-12">
              { competitions.map((comp,index) => (
                // eslint-disable-next-line no-unused-expressions
                <div key={index} className="col-md-6 wow fadeInUp">
                <div className="coupon-list list-wrapper cust_addi_cls">
                  <div className="coupon-wrapper">
                    <div className="row top_gry">
                      <div className="col-md-12 col-sm-12 col-xs-12">
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src={comp.image}
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small />
                        </div>
                      </div>

                      <div className="col-md-12 col-sm-12 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">{comp.title}</a>
                          </h3>
                          <p className="coupn_price">
                            <strong>Coupon Price - $ {comp.price}</strong>
                          </p>
                          <p>
                            {comp.description}
                          </p>
                        </div>
                      </div>
                    </div>

                    <div className="othr_info_detl">
                      <ul>
                        <li>
                          <strong>
                            <span className="fa fa-map-marker" /> Address -{" "}
                          </strong>
                          <span>{comp.address}</span>
                        </li>
                        <li>
                          <strong>
                            <span className="fa fa-users" /> Other Participants
                            -
                          </strong>
                          <span> 5 Out Of 32</span>
                        </li>
                        <li>
                          <strong>
                            <span className="fa fa-ticket" /> Coupon
                            Availability -{" "}
                          </strong>
                          <span> Yes</span>
                        </li>
                      </ul>
                    </div>

                    <Link to="/competetionDetails" className="button_a">
                      {" "}
                      Read More
                    </Link>
                  </div>
                </div>
              </div>
              ))}
              </div>
              
            </div>

            <div className="row">
              <div className="col-md-12">
                <nav className="nav-pagi text-center clearfix">
                  <ul className="pagination">
                    <li>
                      <a href="#" aria-label="Previous">
                        <span aria-hidden="true">«</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">1</a>
                    </li>
                    <li>
                      <a href="#">2</a>
                    </li>
                    <li>
                      <a href="#">3</a>
                    </li>
                    <li>
                      <a href="#">4</a>
                    </li>
                    <li>
                      <a href="#">5</a>
                    </li>
                    <li>
                      <a href="#" aria-label="Next">
                        <span aria-hidden="true">»</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>

            <div className="footer-content">
              <div className="row">
                <div className="col-md-12">
                  <h4>Popular Categories</h4>
                </div>
              </div>

              <div className="row">
                <div className="col-md-2 col-sm-2 col-xs-12">
                  <ul className="customlist">
                    <li>
                      <a href="#">Baby Kids</a>
                    </li>
                    <li>
                      <a href="#">Books &amp; Magazines</a>
                    </li>
                    <li>
                      <a href="#">Computers</a>
                    </li>
                    <li>
                      <a href="#">Cameras</a>
                    </li>
                    <li>
                      <a href="#">Electronics</a>
                    </li>
                  </ul>
                </div>

                <div className="col-md-2 col-sm-2 col-xs-12">
                  <ul className="customlist">
                    <li>
                      <a href="#">Games</a>
                    </li>
                    <li>
                      <a href="#">Gifts</a>
                    </li>
                    <li>
                      <a href="#">Health Beauty</a>
                    </li>
                    <li>
                      <a href="#">Home Garden</a>
                    </li>
                    <li>
                      <a href="#">Home Supplies</a>
                    </li>
                  </ul>
                </div>

                <div className="col-md-2 col-sm-2 col-xs-12">
                  <ul className="customlist">
                    <li>
                      <a href="#">Laptops</a>
                    </li>
                    <li>
                      <a href="#">Entertainetmen</a>
                    </li>
                    <li>
                      <a href="#">Digital Stores</a>
                    </li>
                    <li>
                      <a href="#">Marketplaces</a>
                    </li>
                    <li>
                      <a href="#">Musicians</a>
                    </li>
                  </ul>
                </div>

                <div className="col-md-2 col-sm-2 col-xs-12">
                  <ul className="customlist">
                    <li>
                      <a href="#">Movies &amp; Films</a>
                    </li>
                    <li>
                      <a href="#">Phones</a>
                    </li>
                    <li>
                      <a href="#">Travel</a>
                    </li>
                    <li>
                      <a href="#">Televisions</a>
                    </li>
                    <li>
                      <a href="#">Telegraphers</a>
                    </li>
                  </ul>
                </div>

                <div className="col-md-2 col-sm-2 col-xs-12">
                  <ul className="customlist">
                    <li>
                      <a href="#">Baby Toys</a>
                    </li>
                    <li>
                      <a href="#">Clothings</a>
                    </li>
                    <li>
                      <a href="#">Jewellry</a>
                    </li>
                    <li>
                      <a href="#">Car Supplies</a>
                    </li>
                    <li>
                      <a href="#">Watches</a>
                    </li>
                  </ul>
                </div>

                <div className="col-md-2 col-sm-2 col-xs-12">
                  <ul className="customlist">
                    <li>
                      <a href="#">Glassess</a>
                    </li>
                    <li>
                      <a href="#">Medical</a>
                    </li>
                    <li>
                      <a href="#">Pet Shops</a>
                    </li>
                    <li>
                      <a href="#">LifeStyle</a>
                    </li>
                    <li>
                      <a href="#">Sports</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default SearchCompetetion;
