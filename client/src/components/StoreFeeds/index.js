import React, { Component, Fragment } from "react";

class StoreFeeds extends Component {
  render() {
    return (
      <Fragment>
        <div className="parallax first-section" data-stellar-background-ratio="0.5">
          <div className="container">
            <div className="section-title m30 text-center">
              <h1>Stores</h1>

              <p>
                In dignissim feugiat gravida. Proin feugiat quam sed gravida
                fringilla. Proin quis mauris ut magna fringilla vulputate quis
                non ante. Integer bibendum velit dui. Sed consequat nisi id
                convallis eleifend.{" "}
              </p>
            </div>
          </div>
        </div>

        <div className="container">
          <section className="mid_sec">
            <div className="row">
              <div className="content col-md-8">
                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/str1.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">
                              50% Discount Coupon from CatiLogoms.com
                            </a>
                          </h3>
                          <p>
                            At reasonable prices, quality assurance, 100% secure
                            shopping. Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiolore consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span
                              className="coupon-code"
                              data-original-title=""
                              title=""
                            >
                              2016TATILRA50
                            </span>
                            <span className="show-code">Show Code</span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">Expire : 21/07/2016</small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/store_13.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">Greensoil Printable Coupon Code</a>
                          </h3>
                          <p>
                            100% secure shopping at reasonable prices, quality
                            assurance. Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiolore consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span
                              className="coupon-code"
                              data-original-title=""
                              title=""
                            >
                              PRINT ME
                            </span>
                            <span className="show-code print-code">
                              <i className="fa fa-print" /> Print Coupon
                            </span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">Expire : 21/07/2016</small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/str2.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">MyLogo.com 10$ off for all orders</a>
                          </h3>
                          <p>
                            Win tablet pc discount on your MyLogo com shopping
                            plus free... Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiolore consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span
                              className="coupon-code"
                              data-original-title=""
                              title=""
                            >
                              2016TATILRA33
                            </span>
                            <span className="show-code">Show Code</span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">Expire : 21/07/2016</small>
                          <small className="pull-right">
                            <a href="#">
                              <i className="fa fa-comment-o" /> 5 Comments
                            </a>
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/store_12.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">
                              Follow Skinuque on Facebook get 10% Off
                            </a>
                          </h3>
                          <p>
                            Follow Skinuque on Facebook get 10% Off 100% secure
                            shopping at reasonable prices, quality assurance.
                            Lorem ipsum dolor sit amet, consectetur adipisiciur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span
                              className="coupon-code"
                              data-original-title=""
                              title=""
                            >
                              PRINT ME
                            </span>
                            <span className="show-code print-code">
                              <i className="fa fa-search" /> View Deal
                            </span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">Expire : 21/07/2016</small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/str3.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">44$ off CompanieNamis Discount</a>
                          </h3>
                          <p>
                            Bringing a new breath to the fashion
                            CompanieNamis's.. Lorem ipsum dolor sit amet,
                            consectetur adipisicing elit, sed do eiolore
                            consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span
                              className="coupon-code"
                              data-original-title=""
                              title=""
                            >
                              2016TATILRA56
                            </span>
                            <span className="show-code">Show Code</span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">Expire : 21/07/2016</small>
                          <small className="pull-right">
                            <a href="#">
                              <i className="fa fa-comment-o" /> 21 Comments
                            </a>
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/str5.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">
                              10% Discount Coupon from KnowLogoDesign
                            </a>
                          </h3>
                          <p>
                            Those new members to the site via our instant
                            KnowLogoDesign.com.. Lorem ipsum dolor sit amet,
                            consectetur adipisicing elit, sed do eiolore
                            consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span
                              className="coupon-code"
                              data-original-title=""
                              title=""
                            >
                              2016TATILRA51
                            </span>
                            <span className="show-code">Show Code</span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">Expire : 21/07/2016</small>
                          <small className="pull-right">
                            <a href="#">
                              <i className="fa fa-comment-o" /> 51 Comments
                            </a>
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div
                        className="col-md-4 col-sm-4 col-xs-12 wow bounceIn"
                        data-wow-delay="1.5s"
                      >
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/str4.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">Free Shipping for All Orders</a>
                          </h3>
                          <p>
                            Get free shipping for all your next orders from this
                            store.. Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiolore consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span
                              className="coupon-code"
                              data-original-title=""
                              title=""
                            >
                              2016TATILRA29
                            </span>
                            <span className="show-code">Show Code</span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">Expire : 21/07/2016</small>
                          <small className="pull-right">
                            <a href="#">
                              <i className="fa fa-comment-o" /> 0 Comment
                            </a>
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="coupon-list list-wrapper wow fadeInLeft">
                  <div className="coupon-wrapper">
                    <div className="row">
                      <div className="col-md-4 col-sm-4 col-xs-12">
                        <div className="post-media text-center">
                          <a href="#">
                            <img
                              src="images/str1.jpg"
                              alt=""
                              className="img-responsive"
                            />
                          </a>
                          <small>
                            <a href="#">View Store Coupons</a>
                          </small>
                        </div>
                      </div>

                      <div className="col-md-8 col-sm-8 col-xs-12">
                        <div className="coupon-meta">
                          <h3>
                            <a href="#">$5 for for your next logo design</a>
                          </h3>
                          <p>
                            Get free shipping for all your next orders from this
                            store.. Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiolore consectetur.
                          </p>
                        </div>

                        <div className="showcode">
                          <a href="#" className="code-link">
                            <span
                              className="coupon-code"
                              data-original-title=""
                              title=""
                            >
                              2016TATILRAAA
                            </span>
                            <span className="show-code">Show Code</span>
                          </a>
                        </div>

                        <div className="coupon-bottom clearfix">
                          <small className="pull-left">Expire : 21/07/2016</small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <nav className="nav-pagi">
                  <ul className="pagination">
                    <li>
                      <a href="#" aria-label="Previous">
                        <span aria-hidden="true">«</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">1</a>
                    </li>
                    <li>
                      <a href="#">2</a>
                    </li>
                    <li>
                      <a href="#">3</a>
                    </li>
                    <li>
                      <a href="#">4</a>
                    </li>
                    <li>
                      <a href="#">5</a>
                    </li>
                    <li>
                      <a href="#" aria-label="Next">
                        <span aria-hidden="true">»</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>

              <div className="sidebar col-md-4 col-sm-12 wow fadeInRight">
                <div className="widget custom-widget clearfix">
                  <a href="#">
                    <i className="fa fa-bullhorn alignleft fa-3x" />
                    <h4>Submit a Coupon</h4>
                    <p>Share your code discount's everyone</p>
                  </a>
                </div>

                <div className="widget clearfix">
                  <div className="widget-title">
                    <h4>
                      <span>Best Coupons</span>
                    </h4>
                  </div>

                  <div className="best-coupons">
                    <ul className="customlist">
                      <li>
                        <a href="#">MyLogo.com 10$ off for all orders</a>
                      </li>
                      <li>
                        <a href="#">44$ off CompanieNamis Discount</a>
                      </li>
                      <li>
                        <a href="#">10% Discount Coupon from Gosi</a>
                      </li>
                      <li>
                        <a href="#">Free Shipping for All Orders</a>
                      </li>
                      <li>
                        <a href="#">$5 for for your next logo design</a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className="widget clearfix">
                  <div className="featured hidden-xs">
                    <i className="fa fa-star-o" />
                  </div>
                  <div className="widget-title">
                    <h4>
                      <span>Best Stores</span>
                    </h4>
                  </div>

                  <div className="text-center store-list row">
                    <div className="col-md-6 col-xs-6">
                      <div className="post-media">
                        <a href="#">
                          <img
                            src="images/side.jpg"
                            alt=""
                            className="img-responsive"
                          />
                        </a>
                      </div>
                    </div>
                    <div className="col-md-6 col-xs-6">
                      <div className="post-media">
                        <a href="#">
                          <img
                            src="images/side2.jpg"
                            alt=""
                            className="img-responsive"
                          />
                        </a>
                      </div>
                    </div>

                    <div className="col-md-6 col-xs-6">
                      <div className="post-media">
                        <a href="#">
                          <img
                            src="images/side3.jpg"
                            alt=""
                            className="img-responsive"
                          />
                        </a>
                      </div>
                    </div>

                    <div className="col-md-6 col-xs-6">
                      <div className="post-media">
                        <a href="#">
                          <img
                            src="images/side.jpg"
                            alt=""
                            className="img-responsive"
                          />
                        </a>
                      </div>
                    </div>

                    <div className="col-md-6 col-xs-6">
                      <div className="post-media">
                        <a href="#">
                          <img
                            src="images/side3.jpg"
                            alt=""
                            className="img-responsive"
                          />
                        </a>
                      </div>
                    </div>

                    <div className="col-md-6 col-xs-6">
                      <div className="post-media">
                        <a href="#">
                          <img
                            src="images/side2.jpg"
                            alt=""
                            className="img-responsive"
                          />
                        </a>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="widget clearfix">
                  <div className="widget-title">
                    <h4>
                      <span>Email Newsletter</span>
                    </h4>
                  </div>

                  <div className="newsletter">
                    <p>
                      Your email is safe with us and we hate spam as much as you
                      do. Lorem ipsum dolor sit amet et dolore.
                    </p>
                    <form className="">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Enter your name.."
                      />
                      <input
                        type="email"
                        className="form-control"
                        placeholder="Enter your email.."
                      />
                      <button type="submit" className="btn btn-primary">
                        Subscribe
                      </button>
                    </form>
                  </div>
                </div>

                <img src="images/ad_sid.jpg" />
              </div>
            </div>
          </section>
        </div>
      </Fragment>
    );
  }
}

export default StoreFeeds;
