import { connect } from 'react-redux';
import { createContactRequest } from '../../actions';
import ContactUs from '../../components/ContactUs';

const mapDispatchToProps = {
  createContactRequest
};

export default connect(
  null,
  mapDispatchToProps
)(ContactUs)