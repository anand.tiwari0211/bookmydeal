import { connect } from 'react-redux';
import { getCoupansRequest } from '../../actions';
import CoupanFeeds from '../../components/CouponFeeds';

const mapStateToProps = state => ({
  coupans: state.Coupans.coupans
});

const mapDispatchToProps = {
  getCoupansRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CoupanFeeds)