import { connect } from 'react-redux';
import { onLogoutRequest, getUserRequest } from '../../actions';
import DashboardProfile from '../../components/Dashboard';

const mapStateToProps = state => ({
  user: state.currentUser.user
});

const mapDispatchToProps = {
  onLogoutRequest,
  getUserRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardProfile)