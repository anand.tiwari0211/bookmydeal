import { connect } from 'react-redux';
import { resetPasswordRequest } from '../../actions';
import ResetPassword from '../../components/ForgotPassword/ResetPassword';

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  resetPasswordRequest,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResetPassword)