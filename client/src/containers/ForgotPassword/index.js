import { connect } from 'react-redux';
import { forgotPasswordRequest } from '../../actions';
import ForgotPassword from '../../components/ForgotPassword';

const mapStateToProps = state => ({
});

const mapDispatchToProps = {
  forgotPasswordRequest,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPassword)