import { connect } from 'react-redux';
import { onVerifyAccountRequest } from '../../actions';
import Home from '../../pages/Home';

const mapDispatchToProps = {
  onVerifyAccountRequest,
};

export default connect(
  null,
  mapDispatchToProps
)(Home)