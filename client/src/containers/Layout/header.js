import { connect } from 'react-redux';
import { doModalOpen, onLogoutRequest, getUserRequest } from '../../actions';
import Header from '../../components/Layout/header';

const mapStateToProps = state => ({
  isLoggedIn: state.currentUser.isLoggedIn,
  user: state.currentUser.user
})

const mapDispatchToProps = {
  doModalOpen,
  onLogoutRequest,
  getUserRequest
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header)