import { connect } from 'react-redux';
import { doModalClose, doModalOpen } from '../../actions';
import Modal from '../../components/Modal';

const mapStateToProps = state => ({
  modalIsOpen: state.Modal.modalIsOpen,
  type: state.Modal.type
});

const mapDispatchToProps = {
  doModalClose,
  doModalOpen
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Modal)