import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { getCompetitionsRequest } from '../../actions';
import SearchCompetition from '../../components/SearchCompetetion';

const mapStateToProps = state => ({
  competitions: state.Competitions.competition,
  timestamp: state.Competitions.timestamp
});

const mapDispatchToProps = {
  getCompetitionsRequest
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchCompetition))