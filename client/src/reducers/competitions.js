import * as types from '../actions/constants';

const initialState = {
  competition: [],
  timestamp: new Date()
}

const Competitions = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_COMPETITIONS_SUCCESS:
      return {
        ...state,
        ...action.payload,
        timestamp: new Date()
      };
    default:
    return state
  }
}

export default Competitions;