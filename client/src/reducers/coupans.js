import * as types from '../actions/constants';

const initialState = {
  coupans: []
}

const Coupans = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_COUPANS_SUCCESS:
      return {
        ...state,
        ...action.payload
      };
    default:
    return state
  }
}

export default Coupans;