import * as types from '../actions/constants';

const initialState = {
  modalIsOpen: false,
  type: ''
}

const Modal = (state = initialState, action) => {
  switch (action.type) {
    case types.DO_MODAL_OPEN:
      return {
        ...state,
        ...action.payload
      };
    case types.DO_MODAL_CLOSE:
      return {
        ...state,
        ...action.payload
      };
    default:
    return state
  }
}

export default Modal;