const Competition = require("../model/competition")

const competitionServices = {
  getCompetition: async (req, res) => {
    try{
      const competition = await Competition.find({});
      res.status(200).json({ message: "Competition fetched successfully", competition });
    } catch(err) {
      console.log("Error", err);
      res.status(500).json({ message: "Internal Server error" });
    }
  }
}

module.exports = competitionServices;