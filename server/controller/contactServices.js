const ContactUs = require('../model/contact-us');
const { validationResult } = require('express-validator');

const contactServices = {
  createContact: async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      const contactUs = new ContactUs({ ...req.body });
      await contactUs.save();
      res.status(200).json({ message: "Thank you for contacting us, we will get back to you soon." })
    } catch(err) {
      console.log("Error",err);
      res.status(500).send({ message: "Internal Server Error" });
    }
  }
}

module.exports = contactServices;