const Coupan = require('../model/coupan');

const coupanServices = {
  getCoupan: async (req, res) => {
    try{
      const coupans = await Coupan.find({});
      res.status(200).json({ message: 'Coupan fetched successfully', coupans })
    } catch(err) {
      console.log("Error", err);
      res.status(500).json({ message: "Internal Server Error" })
    }
  }
}

module.exports = coupanServices;