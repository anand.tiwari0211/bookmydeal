const User = require('../model/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const userServices = {
  registerUser: async (req,res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      const hashedPassword = bcrypt.hashSync(req.body.password, 8);
      const user = new User({
        ...req.body,
        role: 'user',
        password: hashedPassword,
        verified: false
      })
      await user.save();
     
      // create a token
      const accountverifytoken = jwt.sign({ email: user.email, password: req.body.password }, process.env.AUTH_TOKEN_SECRET, {
        expiresIn: 86400 // expires in 24 hours
      });

      const msg = {
        to: user.email,
        cc: 'test@bookmydeal.com',
        from: 'support@bookmydeal.com',
        subject: 'Account Verification link',
        html: `Please click here to verify your account:
               <a href='${process.env.LINK_URL}/?verify=${accountverifytoken}'>${accountverifytoken}</a>`,
      };
      await sgMail.send(msg);
      res.status(200).send({ message: "A verification link has been send to your Email account" });
    } catch(err) {
      console.log("Error",err);
      res.status(500).send({ message: "Internal Server Error"});
    }
  },

  loginUser: async (req,res) => {
    try {
      const errorVal = validationResult(req);
      const errors = errorVal.array()
      let email = req.body.email
      let password = req.body.password
      let isVerifiedWithLink = false;
      if(req.headers.linktoken) {
        const decoded = jwt.verify(req.headers.linktoken, process.env.AUTH_TOKEN_SECRET);
        email = decoded.email;
        isVerifiedWithLink = true;
      }

      if(req.headers.accountverifytoken) {
        const decoded = jwt.verify(req.headers.accountverifytoken, process.env.AUTH_TOKEN_SECRET);
        email = decoded.email;
        password = decoded.password;
        await User.findOneAndUpdate({ email }, { $set: { verified: true } });
      }

      if (!email) {
        errors.push({ msg: "Email field is required" })
        return res.status(422).json({ errors })
      }

      if (!password) {
        errors.push({ msg: "password field is required" })
        return res.status(422).json({ errors })
      }

      const user = await User.findOne({ email });
      if (!user) {
        errors.push({ msg: "No user was found with this email" })
        return res.status(422).json({ errors });
      } else if(!user.verified) {
        errors.push({ msg: "User is registered but not verifed yet" })
        return res.status(422).json({ errors });
      } 
      
      if(isVerifiedWithLink) {
        const hashedPassword = bcrypt.hashSync(password, 8);
        await User.findOneAndUpdate({ email }, { $set: { password: hashedPassword } });
      } else {
        const passwordIsValid = bcrypt.compareSync(password, user.password);
        if (!passwordIsValid) {
          errors.push({ msg: "Password doesn't matched" })
          return res.status(401).json({ errors });
        } 
      }
    
      const token = jwt.sign({ id: user._id, role: user.role }, process.env.AUTH_TOKEN_SECRET, {
        expiresIn: 86400 // expires in 24 hours
      });
      
      res.status(200).send({ message: "Success! Login successfull", token, user });
    } catch(err){
      console.log("Error", err)
      res.status(500).send({ message: "Internal Server Error"});
    }
  },

  forgotPassword: async (req,res) => {
    try {
      const linktoken = jwt.sign({ email: req.body.email }, process.env.AUTH_TOKEN_SECRET, {
        expiresIn: 60 * 30 // expires in 30 min
      });
      const msg = {
        to: req.body.email,
        cc: 'test@bookmydeal.com',
        from: 'support@bookmydeal.com',
        subject: 'Email Verification link',
        html: `<a href=${process.env.LINK_URL}>${linktoken}</a>`,
      };
      await sgMail.send(msg);
      res.status(200).json({ message: "Email has been send successfully to your registered account", linktoken });
    } catch(err) {
      console.log("Error", err)
      res.status(500).send({ message: "Internal Server Error"});
    }
  },

  profileUpdate: async (req, res) => {
    try {
      let updateObj;
      if(req.file) {
        updateObj = { ...req.body, profilePicture: req.file.location };   
      } else {
        updateObj = { ...req.body }
      }
      const user = await User.findByIdAndUpdate(req.decoded.id,{
        $set: updateObj
      },{ new : true })
      res.status(200).json({ message: "Success! user profile updated", user })
    } catch(err) {
      console.log("Error", err);
      res.status(400).send({ message: "Internal Server Error" })
    }
  },

  getUser: async (req, res) => {
    try {
      const user = await User.findById(req.decoded.id);
      res.status(200).json({ message: "Success! user profile fetched", user })
    } catch(err) {
      console.log("Error", err);
      res.status(400).send({ message: "Internal Server Error" })
    }
  }
}

module.exports = userServices;