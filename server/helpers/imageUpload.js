const multer = require('multer');
const multerS3 = require('multer-s3');
const s3 = require('../config/aws');
 
const upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'book-my-deal',
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString())
    }
  })
});

module.exports = upload;