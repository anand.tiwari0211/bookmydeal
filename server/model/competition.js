const mongoose = require("mongoose");

const competitionSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true
    },
    price: {
      type: Number,
      required: true
    },
    description: {
      type: String
    },
    address: {
      type: String
    },
    image: {
      type: String
    },
    participants: {
      type: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      }],
      validate: [participantsLimit, '{PATH} exceeds the limit of 32']
    },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

function participantsLimit(val) {
  return val.length <= 32;
}

const Competition = mongoose.model("Competition", competitionSchema);

module.exports = Competition;
