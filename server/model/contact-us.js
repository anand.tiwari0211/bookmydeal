const mongoose = require("mongoose");

const contactUsSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    subject: {
      type: String,
      required: true
    },
    phone: {
      type: Number
    },
    message: {
      type: String
    }
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

const ContactUs = mongoose.model("ContactUs", contactUsSchema);

module.exports = ContactUs;
