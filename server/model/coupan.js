const mongoose = require("mongoose");

const coupanSchema = new mongoose.Schema({},
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

const Coupan = mongoose.model("Coupan", coupanSchema);

module.exports = Coupan;
