const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    name: { 
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true,
      unique: true,
      index: true
    },
    password: {
      type: String,
      required: true
    },
    role: {
      type: String,
      default: 'user'
    },
    terms: {
      type: Boolean,
      required: true
    },
    address: {
      type: String
    },
    profilePicture: {
      type: String
    },
    facebookURL: {
      type: String
    },
    twitterURL: {
      type: String
    },
    linkedInURL: {
      type: String
    },
    website: {
      type: String
    },
    title: {
      type: String
    },
    verified: {
      type: Boolean,
      require: true
    } 
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

const User = mongoose.model("User", userSchema);

module.exports = User;
