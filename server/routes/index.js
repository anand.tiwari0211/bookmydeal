const userServices = require("../controller/userServices");
const coupanServices = require("../controller/coupanServices");
const competitionServices = require("../controller/competitionServices");
const contactServices = require("../controller/contactServices");
const User = require('../model/user');
const auth = require("../auth");
const { check } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const upload = require('../helpers/imageUpload');

const appRoutes = router => {
  router.post(
    "/register",
    [ check("name", "name field cannot be empty").not().isEmpty().trim().escape(),
      check("email").custom(value => {
        if(value) {
          return User.findOne({ email: value }).then(user => {
            if (user) {
              throw new Error('E-mail already in use');
            }
          });
        }
        throw new Error('E-mail field is required');
      }),
      check('password', 'The password must be more than 5 chars long and contain a number')
      .not().isIn(['123', 'password', 'god']).withMessage('Do not use a common word as the password')
      .isLength({ min: 5 })
      .matches(/\d/),
      sanitizeBody("terms").toBoolean()
    ],
    userServices.registerUser
  );

  router.post(
    "/login",
    userServices.loginUser
  );

  router.post(
    "/reset-password",
    [ check("email").custom(value => {
        if(value) {
          return User.findOne({ email: value }).then(user => {
            if (!user) {
              throw new Error('No user registered with this email.');
            }
          });
        }
        throw new Error('E-mail field is required');
      })],
    userServices.forgotPassword
  );
  
  router.put(
    "/user/profile/edit",
    auth.verifyAuthToken,
    upload.single('profile_picture'),
    userServices.profileUpdate
  );

  router.get(
    "/user",
    auth.verifyAuthToken,
    userServices.getUser
  );

  router.get(
    "/coupans",
    coupanServices.getCoupan
  );

  router.get(
    "/competitions",
    competitionServices.getCompetition
  );

  router.post(
    "/contact-us",
    [ check("name", "name field cannot be empty").not().isEmpty().trim().escape(),
      check("email", "email field cannot be empty").isEmail().normalizeEmail(),
      check("subject", "subject field cannot be empty").not().isEmpty().trim().escape(),
      check("message", "message field cannot be empty").not().isEmpty().trim().escape()
    ],
    contactServices.createContact
  )
};

module.exports = appRoutes;
